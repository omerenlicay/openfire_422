package bnet.detaysoft.com.controller;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.core.Response;

import org.apache.commons.lang.StringUtils;
import org.jivesoftware.database.DbConnectionManager;
import org.jivesoftware.openfire.XMPPServer;
import org.jivesoftware.openfire.roster.Roster;
import org.jivesoftware.openfire.roster.RosterItem;
import org.jivesoftware.openfire.user.UserManager;
import org.jivesoftware.openfire.user.UserNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import bnet.detaysoft.com.util.BnetModuleAbstract;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class UsersController extends BnetModuleAbstract {

	private static final Logger Log = LoggerFactory.getLogger(UsersController.class);

	public static final UsersController INSTANCE = new UsersController();

	public static final String GET_USER_ROSTER_MESSAGE = "SELECT distinct ofMessageArchive.fromJID, ofMessageArchive.toJID, ofMessageArchive.body,ofMessageArchive.sentDate as time FROM ofMessageArchive INNER JOIN ofConParticipant ON ofMessageArchive.conversationID = ofConParticipant.conversationID WHERE (ofMessageArchive.stanza IS NOT NULL OR ofMessageArchive.body IS NOT NULL) AND ofMessageArchive.messageID IS NOT NULL AND ofConParticipant.bareJID = ? AND ( ofMessageArchive.toJID = ? OR ofMessageArchive.fromJID = ? ) AND ( ofMessageArchive.toJID =? OR ofMessageArchive.fromJID =?) ORDER BY ofMessageArchive.sentDate desc LIMIT 1 OFFSET 0";
	public static final String MESSAGE_OWNER_JID = "SET ? = ?";
	public static final String GET_USER_ROSTER_MESSAGES = "select x.* from ofMessageArchive x inner join ( SELECT CASE ofMessageArchive.fromJID WHEN ? THEN concat(ofMessageArchive.fromJID, ofMessageArchive.toJID) ELSE concat(?, ofMessageArchive.fromJID) END as 'newjid', max(ofMessageArchive.sentDate) as sentDate FROM ofMessageArchive  INNER JOIN ofConParticipant ON ofMessageArchive.conversationID = ofConParticipant.conversationID  WHERE (ofMessageArchive.stanza IS NOT NULL OR ofMessageArchive.body IS NOT NULL) AND ofMessageArchive.messageID IS NOT NULL AND ofConParticipant.bareJID = ? 	AND (ofMessageArchive.toJID = ? OR ofMessageArchive.fromJID = ?) group by newjid) y on y.newjid = CASE x.fromJID WHEN ? THEN concat(x.fromJID, x.toJID) ELSE concat(?, x.fromJID) END and y.SentDate = x.sentDate";
	private static final String OFFLINE_INFO ="select offlineDate from ofpresence where username = ?";
	
	public static UsersController getInstance() {
		return INSTANCE;
	}

	/**
	 * getMainUserInfo Metodu.
	 * 
	 * @param clid
	 * @param usid
	 * @return
	 */
//	public Response getUserInfo(String getData) {
//		Response response = null;
//		Connection mySQLConnection = null;
//		try {
//			String jsonObject = decodeBase64(getData);
//			JsonObject clientParameter = getProperty(jsonObject);
//			String clid, usid;
//			mySQLConnection = DbConnectionManager.getConnection();
//			PreparedStatement preparedStatement = null;
//			List<String> userIDs = new ArrayList<String>();
//			List<String> userJIDs = new ArrayList<String>();
//			List<String> rosterIDs = new ArrayList<String>();
//			Roster roster = null;
//			clid = getString(clientParameter.get("clid"));
//			usid = getString(clientParameter.get("usid")).toUpperCase();
//			String userId = clid + "#" + usid;
//			String domain = XMPPServer.getInstance().getServerInfo().getXMPPDomain();
//			try {
//				roster = UserManager.getInstance().getUser(userId).getRoster();
//
//			} catch (UserNotFoundException e) {
//				Log.error("UserNotFoundException : " + e);
//			}
//
//			String userID = null;
//			
//	
//			for (RosterItem re : roster.getRosterItems()) {
//				userID = re.getJid().getNode().toLowerCase();
//				if (userID.contains("#")) {
//					userID = userID.split("#")[1];
//				}
//
//				userIDs.add("'" + userID + "'");
//				userJIDs.add("'100#" + userID + "@" + domain + "'");
//				rosterIDs.add(userID);
//			}
//			
//			String inUserIDs = StringUtils.join(userIDs, ',');
//			if (inUserIDs.isEmpty())
//				inUserIDs = usid;
//			
//			/*
//			 * roster bilgisi .net ' servise gonderilir.
//			 * Donen string ile mysql bilgileri birlestirilir.
//			 */
//			String sendRoster = new Gson().toJson(userIDs);
//			String base64Data = ClientRequest.sendRoster(encodeBase64(sendRoster));
//			base64Data = decodeBase64(base64Data);
//			JsonObject object = new JsonParser().parse(base64Data).getAsJsonObject(); //string olarak donen data object'e ceviriyor.
//			JsonArray resultData = null;
//			resultData.add(object);
////			preparedStatement.clearBatch();
//			// Mam mesajlarinin alindigi kisim
//			JsonObject jsonData = null;
//			String rosterUserID = null;
//			String rosterUserCLID = null;
//			String messageBody = null;
//			String sendDate = null;
//			String fromJID = null;
//			String toJID = null;
//			String msUser = null;
//			String ownerUser = userId.toLowerCase() + "@" + domain;
//			List<String> tmpUser=new ArrayList<>();
//			ResultSet messageResult = null;
//			if (resultData.size() > 0 && resultData != null) {
//				preparedStatement = mySQLConnection.prepareStatement(GET_USER_ROSTER_MESSAGES);
//				
//				for(int i=1;i<=7;i++) {
//					preparedStatement.setString(i, userId.toLowerCase() + "@" + domain);
//				}
//				messageResult = preparedStatement.executeQuery();
//
//				while (messageResult.next()) {
//					fromJID = messageResult.getString("fromJID");
//					toJID = messageResult.getString("toJID");
//					messageBody = messageResult.getString("body");
//					sendDate = messageResult.getString("sentDate");
//					for (JsonElement element : resultData) {
//						
//						jsonData = element.getAsJsonObject();
//						rosterUserID = getString(jsonData.get("USID")).toLowerCase();
//						rosterUserCLID = getString(jsonData.get("CLID")).toLowerCase();
//						msUser = rosterUserCLID + "#" + rosterUserID.toLowerCase() + "@" + domain;
//
//							if(!tmpUser.contains(msUser)) {
//						if (ownerUser.equalsIgnoreCase(fromJID) ? msUser.equalsIgnoreCase(toJID)
//								: ownerUser.equalsIgnoreCase(toJID) ? msUser.equalsIgnoreCase(fromJID) : false) {
//							jsonData.addProperty("LMSGC", messageBody.length()<255 ? messageBody : messageBody.substring(0,255));
//							jsonData.addProperty("LMSGD", sendDate);
//							tmpUser.add(msUser);
//							if (ownerUser.equalsIgnoreCase(fromJID)) {
//								jsonData.addProperty("ISOWNER", "true");
//							} else {
//								jsonData.addProperty("ISOWNER", "false");
//							}
//							break;
//						} else {
//							jsonData.addProperty("LMSGC", "");
//							jsonData.addProperty("LMSGD", "");
//							jsonData.addProperty("ISOWNER", "");
//						}
//							}
////						System.out.println(msUser + "  " + fromJID);
////						if (fromJID.equals(msUser) || toJID.equals(msUser)) {
////							jsonData.addProperty("LMSGC", messageBody);
////							jsonData.addProperty("LMSGD", sendDate);
////							if (fromJID.equals(ownerUser))
////								jsonData.addProperty("ISOWNER", "true");
////							else {
////								jsonData.addProperty("ISOWNER", "false");
////							}
////							break;
////						}
//
//					}
//
//				}
//			}
//
//			if (resultData != null) {
//				String gsonString = null;
//				gsonString = encodeBase64(resultData.toString());
//				response = Response.ok(gsonString).build();
//			} else
//				response = Response.ok().build();
//		} catch (Exception e) {
//			Log.error("Get Main User Info Error: " + e);
//		} finally {
//			DbConnectionManager.closeConnection(mySQLConnection);
//		}
//		return response;
//	}

	public Response getLastOnlineInfo(String bpid) {
		Connection con = null;
		ResultSet resultSet;
		String result = "";
		String username ="100#";
		bpid = bpid.toLowerCase(); // eger P1286 ise -> p1286
		username = username.concat(bpid);
		try {
			con = DbConnectionManager.getConnection();
			PreparedStatement preparedStatement = con.prepareStatement(OFFLINE_INFO);
			preparedStatement.setString(1, username);
			resultSet = preparedStatement.executeQuery();
			while(resultSet.next()){
				
				System.out.println(resultSet.getString("offlineDate"));
				result = (resultSet.getString("offlineDate")).toString();
			}
			
			preparedStatement.close();
		}catch(Exception e) {
			System.out.println("OnlineInfo error : " + e);
		}finally {
			DbConnectionManager.closeConnection(con);
		}
		
		return Response.ok(result.toString()).build();
	}
//	/**
//	 * setUserInfo Metodu.
//	 * 
//	 * @param jsonObject
//	 * @throws SQLException
//	 */
//	public Response setUserInfo(String setUserData) throws SQLException {
//		Connection connection = null;
//		Response response = null;
//		try {
//
//			String jsonObject = decodeBase64(setUserData);
//			connection = BnetConnector.getInstance().getSybaseConnection();
//
//			JsonObject clientParameter = getProperty(jsonObject);
//			String clid, usid, lip, guiversion, guiresource;
//
//			clid = getString(clientParameter.get("clid"));
//			usid = getString(clientParameter.get("usid")).toUpperCase();
//			lip = getString(clientParameter.get("pip"));
//			guiversion = getString(clientParameter.get("guiversion"));
//			guiresource = getString(clientParameter.get("guiresource"));
//
//			List<String> parameters = new ArrayList<String>();
//			parameters.add(0, clid);
//			parameters.add(1, usid);
//			ResultSet resultSet = BnetConnector.getInstance().executeQuery(GET_USER_US10, parameters, connection);
//
//			if (resultSet.next()) {
//				parameters.clear();
//				parameters.add(0, lip);
//				parameters.add(1, guiversion);
//				parameters.add(2, guiresource);
//				parameters.add(3, clid);
//				parameters.add(4, usid);
//				BnetConnector.getInstance().executeUpdate(UPDATE_US10, parameters, connection);
//			}
//
//			response = Response.status(200).build();
//		} catch (Exception e) {
//			Log.error("Set User Info Error: " + e);
//		} finally {
//			BnetConnector.getInstance().closeConnection(connection);
//		}
//
//		return response;
//	}

}
