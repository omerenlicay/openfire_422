package bnet.detaysoft.com.controller;

import java.io.UnsupportedEncodingException;
import java.util.Collection;

import org.dom4j.Element;
import org.jivesoftware.openfire.SessionManager;
import org.jivesoftware.openfire.XMPPServer;
import org.jivesoftware.openfire.roster.Roster;
import org.jivesoftware.openfire.roster.RosterItem;
import org.jivesoftware.openfire.roster.RosterManager;
import org.jivesoftware.openfire.session.ClientSession;
import org.jivesoftware.openfire.user.UserNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xmpp.packet.IQ;
import org.xmpp.packet.JID;

public class BroadCastController {

	private static final Logger Log = LoggerFactory.getLogger(BroadCastController.class);
	
	public static void broadCast(ClientSession clientSession,String data)
			throws UserNotFoundException, UnsupportedEncodingException {
		
		// IQ paketi olusturuyoruz.
		IQ iqResponse = new IQ();
		iqResponse.setType(IQ.Type.result);
		
		Element child = iqResponse.setChildElement("pvcommand", "bnet:iq:customiq");
		child.addElement("sendClassName").setText("BNet.SessionViewModel");
		child.addElement("sendMethodName").setText("GetLocationInfo");
		child.addElement("spvcommand").setText(data); // gelen datayi iq paketi olusturup gonderecegiz.

		iqResponse.setType(IQ.Type.get);
		iqResponse.setFrom(XMPPServer.getInstance().getServerInfo().getXMPPDomain());

		// Kullanicinin kendine gonderilmesi.
		JID userJid = clientSession.getAddress();
		iqResponse.setTo(userJid);
		XMPPServer.getInstance().getIQRouter().route(iqResponse);

		RosterManager rosterManager = XMPPServer.getInstance().getRosterManager();
		SessionManager sessionManager = XMPPServer.getInstance().getSessionManager();

		try {
			String userName = clientSession.getUsername();

			// Kullanici roster listesine dagitilmasi
			Roster roster = rosterManager.getRoster(userName);
			Collection<RosterItem> rosterItemList = roster.getRosterItems();
			for (RosterItem rosterItem : rosterItemList) {
				String rosterUserName = rosterItem.getJid().getNode();
				Collection<ClientSession> rosterUserSessions = sessionManager.getSessions(rosterUserName);
				if (!rosterUserSessions.isEmpty()) {
					for (ClientSession rosterClientSession : rosterUserSessions) {
						JID rosterUserJid = rosterClientSession.getAddress();
						iqResponse.setTo(rosterUserJid);
						System.out.println(rosterUserJid);
						XMPPServer.getInstance().getIQRouter().route(iqResponse);
					}
				}
			}
			rosterItemList = null;
		} catch (UserNotFoundException e) {
			String errorMessage = "broadCast islemi yapilamadi.";
			Log.error(errorMessage + " : " + e.getMessage(), e);
		} finally {
			rosterManager = null;
			sessionManager = null;
		}
	
}

}
