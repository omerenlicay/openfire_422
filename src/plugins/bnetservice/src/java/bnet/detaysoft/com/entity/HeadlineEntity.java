package bnet.detaysoft.com.entity;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class HeadlineEntity {

	private String from;

	private List<String> to;

	private String message;

	private boolean isBroadcast=false;

	private boolean isOnline=false;

	private boolean isMobile=true;
	
	private String mobileTitle="Bnet";
	
	private List<String> mobileKeys;

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public List<String> getTo() {
		return to;
	}

	public void setTo(List<String> to) {
		this.to = to;
	}

	public boolean isBroadcast() {
		return isBroadcast;
	}

	public void setBroadcast(boolean isBroadcast) {
		this.isBroadcast = isBroadcast;
	}

	public boolean isOnline() {
		return isOnline;
	}

	public void setOnline(boolean isOnline) {
		this.isOnline = isOnline;
	}

	public boolean isMobile() {
		return isMobile;
	}

	public void setMobile(boolean isMobile) {
		this.isMobile = isMobile;
	}

	public String getMobileTitle() {
		return mobileTitle;
	}

	public void setMobileTitle(String mobileTitle) {
		this.mobileTitle = mobileTitle;
	}

	public List<String> getMobileKeys() {
		return mobileKeys;
	}

	public void setMobileKeys(List<String> mobileKeys) {
		this.mobileKeys = mobileKeys;
	}
	
	
	

}
