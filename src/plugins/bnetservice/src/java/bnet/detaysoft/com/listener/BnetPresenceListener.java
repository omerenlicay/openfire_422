package bnet.detaysoft.com.listener;

import org.jivesoftware.openfire.session.ClientSession;
import org.jivesoftware.openfire.user.PresenceEventListener;
import org.xmpp.packet.JID;
import org.xmpp.packet.Presence;

import bnet.detaysoft.com.controller.PresenceController;

public class BnetPresenceListener implements PresenceEventListener {

	/**
	 * 
	 * Kullanici giris yaptiginda bu metoda duser ve .NET servislerine bu bilgi
	 * gonderilerek us10 ve us11 tablolarina log kayitlari islenir.
	 */
	@Override
	public void availableSession(ClientSession session, Presence presence) {

		PresenceController controller = new PresenceController();

		controller.availableSession(session);
		
		
	}

	@Override
	public void unavailableSession(ClientSession session, Presence presence) {
		// TODO Auto-generated method stub

	}

	@Override
	public void presenceChanged(ClientSession session, Presence presence) {
		// TODO Auto-generated method stub

	}

	@Override
	public void subscribedToPresence(JID subscriberJID, JID authorizerJID) {
		// TODO Auto-generated method stub

	}

	@Override
	public void unsubscribedToPresence(JID unsubscriberJID, JID recipientJID) {
		// TODO Auto-generated method stub

	}

}
