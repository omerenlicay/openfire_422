package bnet.detaysoft.com.controller;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Collection;
import java.util.List;

import javax.ws.rs.core.Response;

import org.apache.commons.lang.StringUtils;
import org.jivesoftware.database.DbConnectionManager;
import org.jivesoftware.openfire.XMPPServer;
import org.jivesoftware.openfire.session.ClientSession;
import org.jivesoftware.openfire.user.UserManager;
import org.jivesoftware.util.JiveGlobals;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xmpp.packet.JID;
import org.xmpp.packet.Message;

import com.google.gson.JsonObject;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;

import bnet.detaysoft.com.entity.HeadlineEntity;
import bnet.detaysoft.com.entity.TokenEntity;
import bnet.detaysoft.com.util.BnetModuleAbstract;

/**
 * 
 * Headline mesajlarinin ofofline tablosuna kay�t islemi icin: package
 * org.jivesoftware.openfire; paketi icindeki 507. satir d�n�sdegeri true olarak
 * ayarlanmistir.
 * 
 * @author hkdem
 *
 */
public class PushNotificationController extends BnetModuleAbstract {

	private static final Logger Log = LoggerFactory.getLogger(PushNotificationController.class);

	private static final String GET_USER_TOKEN = "select * from ofapns where JID in (?)";

	private static final String ADD_TOKEN = "INSERT INTO ofapns (JID, devicetoken) VALUES (?, ?)";

	private static final String UPDATE_TOKEN = "UPDATE ofapns SET devicetoken=? WHERE JID=?";

	private static final String DELETE_TOKEN = "DELETE FROM `ofapns` WHERE `JID`=?";

	private static final PushNotificationController INSTANCE = new PushNotificationController();

	public static PushNotificationController getInstance() {
		return INSTANCE;
	}

	private UserManager userManager;

	public Response headlinePush(HeadlineEntity entity, String token) {

		Response response = null;
		XMPPServer server = XMPPServer.getInstance();

		Message message = new Message();
		message.setBody(entity.getMessage());
		// burada BnetModuleAbstract 'a gelen token' i gonderip JID aliyoruz ve
		// setFrom'a set ediyoruz..
		message.setFrom(getJIDASToken(token));

		message.setType(Message.Type.headline);
		if (!entity.isBroadcast()) {

			for (String userName : entity.getTo()) {
				JID jid = server.createJID(userName, null);
				message.setTo(jid);

				try {
					Collection<ClientSession> clientSessions = server.getSessionManager().getSessions(jid.getNode());
					if (clientSessions.size() > 0) {
						server.getMessageRouter().route(message);
					} else {
						if (!entity.isOnline()) {
							server.getOfflineMessageStore().addMessage(message);

						}
					}

				} catch (Exception e) {
					Log.error(e.getMessage(), e);
				}

			}

			if (entity.isMobile()) {
				sendMobilePush(entity);
			}

			response = Response.ok().build();
		} else {

			userManager = XMPPServer.getInstance().getUserManager();
			List<String> userNames = (List<String>) userManager.getUserProvider().getUsernames();
			for (String userName : userNames) {
				JID jid = server.createJID(userName, null);

				Collection<ClientSession> clientSessions = server.getSessionManager().getSessions(jid.getNode());

				message.setTo(jid);

				if (clientSessions.size() > 0) {
					server.getMessageRouter().route(message);
				} else {
					if (!entity.isOnline()) {
						server.getOfflineMessageStore().addMessage(message);
					}
				}

			}

			if (entity.isMobile()) {
				sendMobilePush(entity);
			}
			response = Response.ok().build();
		}

		return response;
	}

	private void sendMobilePush(HeadlineEntity entity) {
		String firebaseToken = JiveGlobals.getProperty("bnet.module.push.token");
		Connection connection = null;
		try {
			if (entity.getTo().size() > 0) {
				String inUserJIDs = StringUtils.join(entity.getTo(), ',');
				connection = DbConnectionManager.getConnection();
				PreparedStatement statement = connection.prepareStatement(GET_USER_TOKEN);
				statement.setString(1, inUserJIDs);
				ResultSet resultSet = statement.executeQuery();
				while (resultSet.next()) {
					JsonObject firebaseJson = new JsonObject();
					firebaseJson.addProperty("to", resultSet.getString("devicetoken"));
					firebaseJson.addProperty("collapse_key", "type_a");
					JsonObject data = new JsonObject();
					data.addProperty("body", entity.getMessage());
					if (entity.getMobileTitle() != null && !entity.getMobileTitle().isEmpty()) {
						if (entity.getMessage().length() > 11) {
							entity.setMobileTitle(entity.getMessage().substring(0, 11) + "..");
						} else {
							entity.setMobileTitle(entity.getMessage() + "..");
						}
					}
					data.addProperty("title", entity.getMobileTitle());
					for (String key : entity.getMobileKeys()) {
						int val = 1;
						data.addProperty("key_" + val, key);
						val++;
					}

					firebaseJson.add("data", data);

					HttpResponse<String> response = Unirest.post("https://fcm.googleapis.com/fcm/send")
							.header("authorization", "key=" + firebaseToken).header("content-type", "application/json")
							.body(firebaseJson.toString()).asString();
				}

			}

			if (entity.isBroadcast()) {
				userManager = XMPPServer.getInstance().getUserManager();
				List<String> userNames = (List<String>) userManager.getUserProvider().getUsernames();

				String inUserJIDs = StringUtils.join(userNames, ',');
				connection = DbConnectionManager.getConnection();
				PreparedStatement statement = connection.prepareStatement(GET_USER_TOKEN);
				statement.setString(1, inUserJIDs);
				ResultSet resultSet = statement.executeQuery();
				while (resultSet.next()) {
					JsonObject firebaseJson = new JsonObject();
					firebaseJson.addProperty("to", resultSet.getString("devicetoken"));
					firebaseJson.addProperty("collapse_key", "type_a");
					JsonObject data = new JsonObject();
					data.addProperty("body", entity.getMessage());
					data.addProperty("title", entity.getMobileTitle());
					for (String key : entity.getMobileKeys()) {
						int val = 1;
						data.addProperty("key_" + val, key);
						val++;
					}

					firebaseJson.add("data", data);

					HttpResponse<String> response = Unirest.post("https://fcm.googleapis.com/fcm/send")
							.header("authorization", "key=" + firebaseToken).header("content-type", "application/json")
							.body(firebaseJson.toString()).asString();
				}

			}

		} catch (Exception e) {
			Log.error("send push notification error! ", e);
		} finally {
			DbConnectionManager.closeConnection(connection);
		}
	}

	/*
	 * public Response addOrDeleteMobileToken(TokenEntity entity) { Response
	 * response = null; Connection connection = null; PreparedStatement statement =
	 * null; ResultSet resultSet = null; int controll; try { connection =
	 * DbConnectionManager.getConnection(); if
	 * (entity.getOperation().equalsIgnoreCase("add")) { //Eger veritabaninda JID'i
	 * varsa devicetoken update yap. statement =
	 * connection.prepareStatement(UPDATE_TOKEN); statement.setString(1,
	 * entity.getToken()); statement.setString(2, entity.getJid()); controll =
	 * statement.executeUpdate(); //gelen JID veritabaninda yoksa veritabanina yeni
	 * kayit ekle. if(controll == 0) { statement.clearBatch(); statement =
	 * connection.prepareStatement(ADD_TOKEN); statement.setString(1,
	 * entity.getJid()); statement.setString(2, entity.getToken());
	 * statement.executeUpdate(); }
	 * 
	 * } else if (entity.getOperation().equalsIgnoreCase("delete")) { statement =
	 * connection.prepareStatement(DELETE_TOKEN); statement.setString(1,
	 * entity.getJid()); statement.executeUpdate(); }
	 * response=Response.ok().build(); } catch (Exception e) {
	 * response=Response.status(400).build();
	 * Log.error("add or delete token error: ", e); } finally {
	 * DbConnectionManager.closeConnection(connection); }
	 * 
	 * return response;
	 * 
	 * }
	 */

	/**
	 * ofapns tablosundan jid' i ve token'i ekler.
	 * 
	 * @param entity
	 * @return
	 */
	public Response addMobileToken(TokenEntity entity, String token) {
		Response response = null;
		Connection connection = null;
		PreparedStatement statement = null;

		try {
			entity.setJid(getJIDASToken(token));
			connection = DbConnectionManager.getConnection();
			statement = connection.prepareStatement(ADD_TOKEN);
			statement.setString(1, entity.getJid());
			statement.setString(2, entity.getToken());
			statement.executeUpdate();
			response = Response.ok().build();
		} catch (Exception e) {
			response = Response.status(400).build();
			Log.error("set mobiletoken error: ", e);
		} finally {
			DbConnectionManager.closeConnection(connection);
		}
		return response;

	}

	/**
	 * ofapns tablosundan jid' i ve token'i siler
	 * 
	 * @param entity
	 * @return
	 */
	public Response deleteMobileToken(TokenEntity entity, String token) {
		Response response = null;
		Connection connection = null;
		PreparedStatement statement = null;

		try {
			entity.setJid(getJIDASToken(token));
			connection = DbConnectionManager.getConnection();
			statement = connection.prepareStatement(DELETE_TOKEN);
			statement.setString(1, entity.getJid());
			statement.executeUpdate();
			response = Response.ok().build();
		} catch (Exception e) {
			response = Response.status(400).build();
			Log.error("delete mobiletoken error: ", e);
		} finally {
			DbConnectionManager.closeConnection(connection);
		}
		return response;

	}

}
