package bnet.detaysoft.com.restcfg;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response.Status;

import org.jivesoftware.openfire.XMPPServer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import bnet.detaysoft.com.BnetServicePlugin;
import bnet.detaysoft.com.util.BnetModuleAbstract;

import com.sun.jersey.spi.container.ContainerRequest;
import com.sun.jersey.spi.container.ContainerRequestFilter;

/**
 * The Class AuthFilter.
 */
public class BnetAuthFilter extends BnetModuleAbstract implements ContainerRequestFilter {

	/** The log. */
	private static Logger log = LoggerFactory.getLogger(BnetAuthFilter.class);

	/** The http request. */
	@Context
	private HttpServletRequest httpRequest;

	/** The plugin. */
	private BnetServicePlugin plugin = (BnetServicePlugin) XMPPServer.getInstance().getPluginManager()
			.getPlugin("bnetservice");

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.sun.jersey.spi.container.ContainerRequestFilter#filter(com.sun.jersey
	 * .spi.container.ContainerRequest)
	 */
	@Override
	public ContainerRequest filter(ContainerRequest containerRequest) throws WebApplicationException {

		if ("OPTIONS".equals(containerRequest.getMethod())) {
			return containerRequest;
		}

		// Gelen istekte 'ki authorization icerigini aliyoruz.(token gelecek bize)
		String data = containerRequest.getHeaderValue("authorization");
		String token;
		String validToken;
				
		// null geldiyse hata UNAUTHORIZED, yetkisiz hatasi ver.
		if (data == null) {
			throw new WebApplicationException(Status.UNAUTHORIZED);
		}

		/*
		 *  Eger ticket bos degilse,
		 *  base64 gelen data 100:token olarak decode edilip token alinip 
		 *  keycloak dogrulamasi yapilacak.
		 */
		if (data != null && !data.equals("bobili")) {
				token = decodeBase64(data);
				validToken = splitToken(token,1);
				if (!validateUserOnKeycloak(validToken)) {
					log.error("bnetservice authfilter error");
					throw new WebApplicationException(Status.UNAUTHORIZED);
				}
		}
		
		return containerRequest;
	}
}
