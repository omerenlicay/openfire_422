package bnet.detaysoft.com.entity;

public class PresenceEntity {
	
	private String publicIp;
	
	private String sessionId;
	
	private String usid;
	
	private String guisrc;
	
	

	public PresenceEntity(String publicIp, String sessionId, String usid, String guisrc) {
		super();
		this.publicIp = publicIp;
		this.sessionId = sessionId;
		this.usid = usid;
		this.guisrc=guisrc;
	}

	public String getUsid() {
		return usid;
	}

	public void setUsid(String usid) {
		this.usid = usid;
	}

	public String getPublicIp() {
		return publicIp;
	}

	public void setPublicIp(String publicIp) {
		this.publicIp = publicIp;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public String getGuisrc() {
		return guisrc;
	}

	public void setGuisrc(String guisrc) {
		this.guisrc = guisrc;
	}
	
	
	

}
