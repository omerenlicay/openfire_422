package bnet.detaysoft.com.services;

import javax.annotation.PostConstruct;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import bnet.detaysoft.com.controller.UsersController;

@Path("bnetservice/v1/user")
public class UserService {

	private UsersController userControll;

	@PostConstruct
	public void init() {
		userControll = UsersController.getInstance();
	}

	/**
	 * 
	 * Backend .Net servislerinden locnm,begdt,begtm,guiversion verileri alınarak,
	 * kullanıcıdan gelen clid ve usid gönderilecek dataya eklenerek, ofroster
	 * tablosundan lastmsg,lastmsgdt alanı eklenerek, son olarak gelen usid ile
	 * ofrosterden çekilen jid aynı ise isowner true eklenerek cevap dondurulur.
	 * 
	 * Data: {"CLID": "100","USID": "00000001","LOCNM": "Detaysoft
	 * Teknokent","BEGDT": "20180424","BEGTM": "173902","GUIVERSION":
	 * "5.4.9_D","LMSGC": "","LMSGD": "","ISOWNER": ""}
	 * 
	 * 
	 * Client:
	 * Chat s�ralamasini LMSGD alani ile yapacak. 
	 * @param clid
	 * @param usid
	 * @return
	 * @throws Exception
	 */
	@POST
	@Path("/getinfo")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.TEXT_PLAIN)
	public Response getMainUserInfo(@FormParam("getinfo") String getinfo) throws Exception {

		// return userControll.getUserInfo(getinfo);

		return null;
	}

	/**
	 * client bu serviste kendi client versiyonunu ve resource unu servise
	 * göndererek kaydeder
	 * 
	 * Data:
	 * {"clid":"100","usid":"00001202","guiversion":"5.4.9_W","guiresource":"BNet"}
	 * 
	 * 
	 * @param setUserData
	 * @return
	 * @throws Exception
	 */
	@POST
	@Path("/setinfo")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.TEXT_PLAIN)
	public Response setMainUserInfo(@FormParam("setdata") String setUserData) throws Exception {

		return null;
	}

	/**
	 * Data: bpid
	 * 
	 * Rosterdaolmayan kullan�c�lar�n presence bilgisini almak i�in kullan�lacak servis.
	 * Eski sistemde bunun kar��l��� ...presence/status servisiydi
	 * @param getbpid
	 * @return
	 * @throws Exception
	 */
	@POST
	@Path("/presence")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.TEXT_PLAIN)
	public Response getOffline(@FormParam("getbpid") String getbpid) throws Exception {
//			var jid = authHeaderToJid(containerRequest.getHeaderValue("authorization"));
		
		return userControll.getLastOnlineInfo(getbpid);

	}
}