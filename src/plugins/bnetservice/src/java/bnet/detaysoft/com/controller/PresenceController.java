package bnet.detaysoft.com.controller;

import org.jivesoftware.openfire.session.ClientSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import bnet.detaysoft.com.entity.PresenceEntity;
import bnet.detaysoft.com.util.PresenceUtils;

public class PresenceController extends PresenceUtils {

	private static final Logger Log = LoggerFactory.getLogger(PresenceController.class);

	public boolean availableSession(ClientSession session) {
		boolean status = false;

		try {
			String publicip, sessid, usid, guisrc;

			publicip = getPublicIP(session);
			sessid = session.getStreamID().getID();
			usid = session.getAddress().getNode();
			guisrc = session.getAddress().getResource();
			PresenceEntity presenceEntity = new PresenceEntity(publicip, sessid, usid, guisrc);

			entityToJson(presenceEntity);
		} catch (Exception e) {
			Log.error("");
		}

		return status;
	}

}
