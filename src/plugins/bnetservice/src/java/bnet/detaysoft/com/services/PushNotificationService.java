package bnet.detaysoft.com.services;

import javax.annotation.PostConstruct;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.sun.jersey.spi.container.ContainerRequest;

import bnet.detaysoft.com.controller.PushNotificationController;
import bnet.detaysoft.com.entity.HeadlineEntity;
import bnet.detaysoft.com.entity.TokenEntity;
import bnet.detaysoft.com.util.BnetModuleAbstract;


@Path("bnetservice/v1/push")
public class PushNotificationService {

	private PushNotificationController controller;

	@PostConstruct
	private void init() {
		controller = PushNotificationController.getInstance();
	}

	/**
	 * 
	 * Headline push servisi
	 * @param entity
	 * @return
	 */
	@POST
	@Path("/headline")
	//@Produces(MediaType.APPLICATION_JSON)
	//@Consumes(MediaType.APPLICATION_JSON)
	public Response headlinePush(@HeaderParam("Authorization") String authorization,final HeadlineEntity entity) {
					
		return controller.headlinePush(entity, authorization); 
	}
	
	/**
	 * ofapns tablosuna JID,devicetoken bilgilerini ekle islemini yapan servis.
	 * @param entity
	 * @return
	 */
	@POST
	@Path("/settoken")
//	@Produces(MediaType.APPLICATION_JSON)
//	@Consumes(MediaType.APPLICATION_JSON)
	public Response setToken(@HeaderParam("Authorization") String authorization, final TokenEntity entity) {
		return controller.addMobileToken(entity, authorization);
	}
	
	
	/**
	 * ofapns tablosuna JID,devicetoken bilgilerini sil islemini yapan servis.
	 * @param entity
	 * @return
	 */
	@POST
	@Path("/deletetoken")
	//@Produces(MediaType.APPLICATION_JSON)
	//@Consumes(MediaType.APPLICATION_JSON)
	public Response deleteToken(@HeaderParam("Authorization") String authorization, final TokenEntity entity) {
		return controller.deleteMobileToken(entity,authorization);
	}

}
