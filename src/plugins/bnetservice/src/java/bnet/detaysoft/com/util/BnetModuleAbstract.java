package bnet.detaysoft.com.util;

import java.io.UnsupportedEncodingException;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.codec.binary.Base64;
import org.dom4j.Element;
import org.jivesoftware.openfire.IQRouter;
import org.jivesoftware.openfire.XMPPServer;
import org.jivesoftware.util.JiveGlobals;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xmpp.packet.IQ;
import org.xmpp.packet.JID;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonNull;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.sun.jersey.spi.container.ContainerRequest;

public abstract class BnetModuleAbstract {

	private static final Logger Log = LoggerFactory.getLogger(BnetModuleAbstract.class);

	/**
	 * Kullanicidan gelen json verinin parse edildigi metod.
	 * 
	 * @param jsonObject
	 * @return
	 */
	public static JsonObject getProperty(String jsonObject) {
		// Gelen parametler JsonObject'e parse ediliyor
		JsonParser jsonParser = new JsonParser();
		JsonObject object = jsonParser.parse(jsonObject).getAsJsonObject();
		return object;
		
	}
	


	/**
	 * Bu method parametre olarak gelen 
	 * 100:token 'in base64decode halini alip,
	 * bazi gerekli islemleri yaparak jid donderir.
	 * 
	 * @param base64Data
	 * @return
	 */
	public String getJIDASToken(String token) {
		
		String decodedToken = decodeBase64(token);
		token = splitToken(decodedToken, 1);
		
		token = getKeycloakUserBpid(token);
		JsonElement jsonElement = new JsonParser().parse(token);
		JsonObject jsonObject = jsonElement.getAsJsonObject();

		String sub = getString(jsonObject.get("sub"));
		String bpid = splitToken(sub, 2);
		
		XMPPServer server = XMPPServer.getInstance();
		JID jid = server.createJID(bpid, null);	
		
		return jid.toString();
	}
	
	public String entityToJson(Object object)
	{
		String jsonInString=null;
		
		ObjectMapper mapper = new ObjectMapper();
		
		try {
			jsonInString = mapper.writeValueAsString(object);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return jsonInString;
	}

	/**
	 * Verinin base64 ten String e donusturen metod.
	 * 
	 * @param base64Data
	 * @return
	 */
	public static String decodeBase64(String base64Data) {
		String returnData = null;
		try {
			byte[] byteData = Base64.decodeBase64(base64Data.getBytes("UTF-8"));
			returnData = new String(byteData, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return returnData;
	}
	
	/**
	 * Verinin base64 e donusturen metod.
	 * 
	 * @param base64Data
	 * @return
	 */
	public static String encodeBase64(String base64Data) {
		String returnData = null;
		Compress compress = new Compress();
		try {
			byte[] byteData = Base64.encodeBase64(compress.compress(base64Data.getBytes("UTF-8")));
			returnData = new String(byteData, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return returnData;
	}

	/**
	 * Bu method auth i�leminde gelen datayi split edip
	 * token'i almak ve token icerisinde gelen bpid'yi 
	 * almak icin yazilmistir.
	 * 
	 * @param token
	 * @return
	 */
	public static String splitToken(String token,int step) {
		String[] splitToken;
		splitToken = token.split(":");

		return splitToken[step];
	}
	
	/**
	 * Gelen parametlerin filtreden gectigi yer
	 *
	 * @param element
	 * @return
	 */
	protected static String getString(JsonElement element) {
		String returnString = "";
		if (element != null && !(element instanceof JsonNull)) {
			returnString = element.getAsString();
			returnString = returnString.replaceAll("'", "\'");
		}
		return returnString;
	}
	
	
	/**
	 * Calistirilan SQL sorgusunun sonucunun Json'a parse edilmesi
	 *
	 * @param resultSet
	 * @return
	 * @throws SQLException
	 */
	public static JsonArray toJson(ResultSet resultSet) throws SQLException {
		JsonArray jsonArray = new JsonArray();
		JsonObject object;
		ResultSetMetaData metaData = resultSet.getMetaData();
		while (resultSet.next()) {
			object = new JsonObject();
			for (int i = 1; i <= metaData.getColumnCount(); i++) {
				object.addProperty(metaData.getColumnName(i),
						(resultSet.getString(i) != null ? resultSet.getString(i) : ""));
			}
			jsonArray.add(object);
		}
		if (jsonArray.size() <= 0)
			jsonArray = null;
		return jsonArray;
	}

	/**
	 * Select sorgusu icin kullanilir calistirilan sql sorgusunu Json'a cevirip geri
	 * dondurur (PreparedStatement icin kullanilir) <br/>
	 * packetId = Gonderilecek paketin id'si <br/>
	 * packetFrom = Kime gonderilecegi <br/>
	 * clientClassMethod = Client'dan gelen ve tekrar client'a gidicek olan class ve
	 * method adi <br/>
	 * resultSet = SQL'den donen Sonuc kumesi
	 *
	 * @param packetId
	 * @param packetFrom
	 * @param clientClassMethod
	 * @param resultSet
	 * @throws SQLException
	 */
	protected static void toJsonAndSendPacket(String packetId, String packetFrom, String clientClassMethod,
			Object object) throws SQLException {
		JsonArray jsonArray = null;

		if (object instanceof ResultSet) {
			jsonArray = toJson((ResultSet) object);
		} else if (object instanceof JsonArray) {
			jsonArray = (JsonArray) object;
		}

		if (jsonArray != null) {
			sendResponseIq(packetId, packetFrom, jsonArray, clientClassMethod);
		}
	}

	/**
	 * Openfire uzerinden gonderilen paketi client'a gonderir
	 *
	 * @param packetId
	 * @param packetTo
	 * @param resultObject
	 * @param clientClassMethod
	 * @throws SQLException
	 */
	protected static void sendResponseIq(String packetId, String packetTo, JsonArray resultObject,
			String clientClassMethod) throws SQLException {
		Gson gson = new Gson();
		String gsonString = gson.toJson(resultObject);

		IQRouter iqRouter = XMPPServer.getInstance().getIQRouter();

		try {
			Compress compresser = new Compress();
			byte[] data = Base64.encodeBase64(compresser.compress(gsonString.getBytes("UTF-8")));
			gsonString = new String(data, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		// gsonString = Base64.encodeBytes(compresser.compress(gsonString.getBytes()));

		createIQPacket(iqRouter, clientClassMethod, packetId, packetTo, gsonString, IQ.Type.result);
	}

	/**
	 * IQ paketinin olusturulmasinda kullanilir <br/>
	 * packetId = Gonderilecek paketin id'si <br/>
	 * packetTo = Kime gonderilecegi <br/>
	 * clientClassMethod = Client'dan gelen ve tekrar client'a gidicek olan class ve
	 * method adi <br/>
	 * type = Gonderilecek IQ Type'i (IQ.Type) <br/>
	 * logStatus = Loglanma durumunun Error veya Success olucagini belirler
	 *
	 * @param iqRouter
	 * @param clientClassMethod
	 * @param packetId
	 * @param packetTo
	 * @param type
	 * @param logStatus
	 */
	private static void createIQPacket(IQRouter iqRouter, String clientClassMethod, String packetId, String packetTo,
			String gsonString, IQ.Type type) {
		IQ iqResponse = new IQ();
		iqResponse.setType(type);
		Element child = iqResponse.setChildElement("pvcommand", "detayopenfireplugin:iq:customiq");
		child.addElement("spvcommand").setText(gsonString);

		child.addElement("sendClassName").setText(clientClassMethod.split("#")[0]);
		child.addElement("sendMethodName").setText(clientClassMethod.split("#")[1]);
		child.addElement("clientKey").setText(clientClassMethod.split("#")[2]);

		iqResponse.setID(packetId);
		iqResponse.setTo(packetTo);
		iqResponse.setType(IQ.Type.get);
		iqResponse.setFrom(XMPPServer.getInstance().getServerInfo().getXMPPDomain());
		iqRouter.route(iqResponse);
	}
	
	
	/**
	 * 
	 * kullanicidan gelen tokeni dogrulayan method.
	 * 
	 * @param token
	 * @return
	 */
	public boolean validateUserOnKeycloak(String token) {

		String validateUrl = JiveGlobals.getProperty("bnet.keycloak.validateUrl",
				"http://bnet.detaysoft.com/auth/realms/master/protocol/openid-connect/token/introspect");

		boolean isStatus = false;
		try {
			String clientId = JiveGlobals.getProperty("bnet.keycloak.clientid", "test");

			String authHeader = "account:" + clientId;

			HttpResponse<String> response = Unirest.post(validateUrl)
					.header("content-type", "application/x-www-form-urlencoded")
					.header("authorization", "Basic " + new String(Base64.encodeBase64(authHeader.getBytes("UTF-8"))))
					.body("token=" + token).asString();
			if (response.getStatus() == 200) {
				isStatus = responseBodyParse(response.getBody());
			}
		} catch (Exception e) {
			Log.error("validate keycloak token error : ", e);
		}
		return isStatus;
	}

	
	/**
	 * keycloak'tan gelen body'nin active parametresine bakar.
	 * "active": false ise valid olmamis demektir.
	 * 
	 * @param responseBody
	 * @return
	 */
	public boolean responseBodyParse(String responseBody) {

		boolean status = false;

		JsonElement jsonElement = new JsonParser().parse(responseBody);
		JsonObject jsonObject = jsonElement.getAsJsonObject();

		if (jsonObject.get("active").toString().equals("true"))
			status = true;

		return status;

	}
	
	
	/**
	 * Bu method ; kullanicinin kim oldugunu bulmak icin yazilmistir.
	 * 
	 * kullanicidan gelen tokeni dogrulayip body kismini donen method.
	 * 
	 * @param token
	 * @return
	 */
	public String getKeycloakUserBpid(String token) {

		HttpResponse<String> response = null;
		
		String validateUrl = JiveGlobals.getProperty("bnet.keycloak.validateUrl",
				"http://bnet.detaysoft.com/auth/realms/master/protocol/openid-connect/token/introspect");

		boolean isStatus = false;
		try {
			String clientId = JiveGlobals.getProperty("bnet.keycloak.clientid", "test");

			String authHeader = "account:" + clientId;

			 response = Unirest.post(validateUrl)
					.header("content-type", "application/x-www-form-urlencoded")
					.header("authorization", "Basic " + new String(Base64.encodeBase64(authHeader.getBytes("UTF-8"))))
					.body("token=" + token).asString();
			
		} catch (Exception e) {
			Log.error("getKeycloakUserBpid keycloak token error : ", e);
		}
		
		return response.getBody();
	
	}
	
	
	public String getServiceSecret()
	{
		return encodeBase64("100:OPENFIRE:bobili");
	}


}
