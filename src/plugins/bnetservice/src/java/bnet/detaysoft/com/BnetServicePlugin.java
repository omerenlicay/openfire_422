package bnet.detaysoft.com;


import java.io.File;

import org.jivesoftware.openfire.container.Plugin;
import org.jivesoftware.openfire.container.PluginManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BnetServicePlugin implements Plugin {

	private static final Logger logger = LoggerFactory.getLogger(BnetServicePlugin.class);

	public static boolean isProject;

	public static boolean isProject() {
		return isProject;
	}

	public static void setProject(boolean isProject) {
		BnetServicePlugin.isProject = isProject;
	}

	@Override
	public void initializePlugin(PluginManager manager, File pluginDirectory) {

		try {
			
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Detay Openfire Service InitializePlugin " + e.getLocalizedMessage(), e);
			destroyPlugin();

		}

	}

	@Override
	public void destroyPlugin() {

	}


}