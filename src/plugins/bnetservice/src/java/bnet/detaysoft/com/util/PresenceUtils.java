package bnet.detaysoft.com.util;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.URL;

import org.jivesoftware.openfire.http.HttpBindManager;
import org.jivesoftware.openfire.http.HttpSession;
import org.jivesoftware.openfire.session.ClientSession;

public class PresenceUtils extends BnetModuleAbstract{
	
    public static String publicIP;

	
	 /**
     * Server'in publicIP si
     * @return
     */
    public synchronized static String getDefaultPublicIP() {
    	try{
	        URL whatismyip = new URL("http://ipecho.net/plain");
	        BufferedReader in = new BufferedReader(new InputStreamReader(whatismyip.openStream()));
	
	        String ip = in.readLine(); 
	        
	        return ip;
    	}catch(Exception e){
    		return "127.0.0.1";
    	}
        
    }
    
    
    
    
    /**
     * Kullanicinin public Ip si
     * 
     * @param clientSession
     * @return
     */
    public synchronized String getPublicIP(ClientSession clientSession) {
        String clientHostIP;

        HttpSession httpSession = HttpBindManager.getInstance().getSessionManager().getSession(clientSession.getStreamID().getID());
        if (httpSession != null) {
            if (httpSession.getSessionData("http-ip") != null) {
                clientHostIP = httpSession.getSessionData("http-ip").toString();
            } else {
                clientHostIP = "127.0.0.1";
            }
        } else {
        	try {
        		if (isLinkLocalIPv4Address(InetAddress.getByName(clientSession.getHostAddress()))){
        			clientHostIP = publicIP;
        		}else{
        			clientHostIP = clientSession.getHostAddress();
        		}
                
            } catch (Exception e) {
                clientHostIP = "127.0.0.1";
            }
        }
        
        return clientHostIP;
    }
    
    
    /**
     * IP'nin local mi yoksa global mi?
     * Sorusuna cevap veren yer
     * 
     * @param add
     * @return
     */
    private synchronized boolean isLinkLocalIPv4Address(InetAddress add) {
        if (add instanceof Inet4Address) {
            byte address[] = add.getAddress();
            if ((address[0] & 0xFF) == 10) {
                return true;
            } else if ((address[0] & 0xFF) == 172 && (address[1] & 0xFF) >= 16 && address[1] <= 31) {
                return true;
            } else if ((address[0] & 0xFF) == 192 && (address[1] & 0xFF) == 168) {
                return true;
            } else if (add.getHostAddress().contains("127.0.0.1")) {
                return true;
            } else {
                return false;
            }
        }
        return false;
    }

}
