package bnet.detaysoft.com.packet.interceptor;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.jivesoftware.database.DbConnectionManager;
import org.jivesoftware.openfire.interceptor.PacketInterceptor;
import org.jivesoftware.openfire.interceptor.PacketRejectedException;
import org.jivesoftware.openfire.session.Session;
import org.xmpp.packet.Message;
import org.xmpp.packet.Packet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BnetPacketInterceptor implements PacketInterceptor {

	private static String UPDATE_MSG_BODY = "update ofroster set lastmsg = ?, lastmsgdt = ? where username = ? and jid = ?";
	private static final Logger Log = LoggerFactory.getLogger(BnetPacketInterceptor.class);

	@Override
	public void interceptPacket(Packet packet, Session session, boolean incoming, boolean processed)
			throws PacketRejectedException {
		if (packet instanceof Message) {
			Message message = (Message) packet;
			Connection connection = null;
			try {
				long epoch = System.currentTimeMillis() / 1000; // epoch convertor'dan java olani aldik.

				if (message.getBody() != null) {

					connection = DbConnectionManager.getConnection();
					PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_MSG_BODY);
					preparedStatement.setString(1, message.getBody());
					preparedStatement.setString(2, String.valueOf(epoch));
					preparedStatement.setString(3, message.getFrom().getNode());
					preparedStatement.setString(4, message.getTo().toBareJID());
					preparedStatement.executeUpdate();
					preparedStatement.close();

				}
			} catch (Exception e) {

				Log.error("message package error :  " + e);

			} finally {
				DbConnectionManager.closeConnection(connection);
			}
		}
		if (incoming && processed) {
//			System.out.println("Gelen: " + packet.toXML());
		}

	}
}