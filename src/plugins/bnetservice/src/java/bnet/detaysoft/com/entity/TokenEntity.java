package bnet.detaysoft.com.entity;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class TokenEntity {

	
	private String jid;
	
	private String token;
	

	public String getJid() {
		return jid;
	}

	public void setJid(String jid) {
		this.jid = jid;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}
	
	
}
