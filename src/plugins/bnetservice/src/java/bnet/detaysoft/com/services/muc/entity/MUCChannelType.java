package bnet.detaysoft.com.services.muc.entity;

public final class MUCChannelType {
    public static final String PUBLIC = "public";
    public static final String ALL = "all";

    private MUCChannelType() {
    }
}
