package bnet.detaysoft.com.entity;

import java.io.Serializable;

public class UserEntity
  implements Serializable
{
  private String clid;
  private String bpid;
  private String bpnm;
  
  public String getClid()
  {
    return this.clid;
  }
  
  public void setClid(String clid)
  {
    this.clid = clid;
  }
  
  public String getBpid()
  {
    return this.bpid;
  }
  
  public void setBpid(String bpid)
  {
    this.bpid = bpid;
  }
  
  public String getBpnm()
  {
    return this.bpnm;
  }
  
  public void setBpnm(String bpnm)
  {
    this.bpnm = bpnm;
  }
}
