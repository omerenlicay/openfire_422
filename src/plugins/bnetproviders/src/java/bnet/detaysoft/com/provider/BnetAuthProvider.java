package bnet.detaysoft.com.provider;

import bnet.detaysoft.com.controller.UserController;
import bnet.detaysoft.com.entity.UserEntity;
import bnet.detaysoft.com.http.HttpClient;


import org.jivesoftware.openfire.auth.AuthProvider;
import org.jivesoftware.openfire.auth.ConnectionException;
import org.jivesoftware.openfire.auth.InternalUnauthenticatedException;
import org.jivesoftware.openfire.auth.UnauthorizedException;
import org.jivesoftware.openfire.user.UserNotFoundException;
import org.jivesoftware.util.JiveGlobals;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BnetAuthProvider implements AuthProvider {
	private static final Logger Log = LoggerFactory.getLogger(BnetAuthProvider.class);
	private static boolean authStatus = true;

	public BnetAuthProvider() {
		authStatus = JiveGlobals.getBooleanProperty("xmpp.auth.anonymous", true);
	}

	public void authenticate(String username, String password)
			throws UnauthorizedException, ConnectionException, InternalUnauthenticatedException {

		String authJids = JiveGlobals.getProperty("admin.authorizedJIDs");
		String authSecret = JiveGlobals.getProperty("admin.secret");
		if (authJids.contains(username)) {
			
			if (!authSecret.equals(password))
				throw new UnauthorizedException();

		} else {
			if (!authStatus) {
				HttpClient client = new HttpClient();
				UserController controller = UserController.getInstance();
				UserEntity entity = controller.getUser(username);
				if (entity == null) {
					throw new UnauthorizedException();
				} else if (!client.validateUserOnKeycloak(password)) {
					throw new UnauthorizedException();
				}
			}
		}
	}

	@Deprecated
	public String getPassword(String username) throws UserNotFoundException, UnsupportedOperationException {
		return null;
	}

	@Deprecated
	public void setPassword(String username, String password)
			throws UserNotFoundException, UnsupportedOperationException {
	}

	@Deprecated
	public boolean supportsPasswordRetrieval() {
		return false;
	}

	public boolean isScramSupported() {
		return false;
	}

	public String getSalt(String username) throws UnsupportedOperationException, UserNotFoundException {
		return null;
	}

	public int getIterations(String username) throws UnsupportedOperationException, UserNotFoundException {
		return 0;
	}

	public String getServerKey(String username) throws UnsupportedOperationException, UserNotFoundException {
		return null;
	}

	public String getStoredKey(String username) throws UnsupportedOperationException, UserNotFoundException {
		return null;
	}
}
