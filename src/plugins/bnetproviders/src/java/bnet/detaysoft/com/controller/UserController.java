package bnet.detaysoft.com.controller;

import bnet.detaysoft.com.entity.UserEntity;
import bnet.detaysoft.com.http.HttpClient;
import bnet.detaysoft.com.utils.Compress;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;
import org.apache.commons.codec.binary.Base64;

public class UserController
{
  private static UserController instance = null;
  private static TreeMap<String, UserEntity> usersMap = null;
  
  public static UserController getInstance()
  {
    if (instance == null)
    {
      instance = new UserController();
      usersMap = new TreeMap<String, UserEntity>();
      setupData();
    }
    return instance;
  }
  
  private static void setupData()
  {
    HttpClient client = new HttpClient();
    String requestResult = client.userDataRequest();
    
    Compress compress = new Compress();
    byte[] decompressResult = null;
    try
    {
      decompressResult = compress.deCompress(Base64.decodeBase64(requestResult.getBytes("UTF-8")));
      
      JsonParser jsonParser = new JsonParser();
      JsonObject object = jsonParser.parse(new String(decompressResult, "UTF-8")).getAsJsonObject();
      System.out.println(new String(object.toString()));
      JsonArray data = object.getAsJsonArray("Data");
      for (JsonElement user : data)
      {
        JsonObject jsonObject = user.getAsJsonObject();
        if (!jsonObject.get("BPID").toString().contains("CUS"))
        {
          UserEntity userEntity = new UserEntity();
          userEntity.setClid(jsonObject.get("CLID").toString().replaceAll("\"", ""));
          userEntity.setBpid(jsonObject.get("BPID").toString().replaceAll("\"", ""));
          userEntity.setBpnm(jsonObject.get("BPNM").toString().replaceAll("\"", ""));
          usersMap.put(userEntity.getBpid(), userEntity);
        }
      }
    }
    catch (UnsupportedEncodingException e)
    {
      e.printStackTrace();
    }
  }
  
  public List<UserEntity> getUsers()
  {
    List<UserEntity> usernames = new ArrayList<UserEntity>(usersMap.values());
    return usernames;
  }
  
  public UserEntity getUser(String bpid)
  {
    return (UserEntity)usersMap.get(bpid);
  }
}
