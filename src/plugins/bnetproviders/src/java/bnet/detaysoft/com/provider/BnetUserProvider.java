package bnet.detaysoft.com.provider;

import bnet.detaysoft.com.controller.UserController;
import bnet.detaysoft.com.entity.UserEntity;
import java.io.PrintStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import org.jivesoftware.database.DbConnectionManager;
import org.jivesoftware.openfire.auth.AuthFactory;
import org.jivesoftware.openfire.user.User;
import org.jivesoftware.openfire.user.UserAlreadyExistsException;
import org.jivesoftware.openfire.user.UserCollection;
import org.jivesoftware.openfire.user.UserNotFoundException;
import org.jivesoftware.openfire.user.UserProvider;
import org.jivesoftware.util.JiveGlobals;
import org.jivesoftware.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BnetUserProvider
  implements UserProvider
{
  private static final Logger Log = LoggerFactory.getLogger(BnetUserProvider.class);

  
  public User loadUser(String username)
    throws UserNotFoundException
  {
	  String authJids = JiveGlobals.getProperty("admin.authorizedJIDs");
    if (authJids.contains(username))
    {
      User user = new User(username, "admin", "deneme@mail", new Date(), new Date());
      return user;
    }
    User user = null;
    UserEntity userEntity = null;
    try
    {
      UserController userController = UserController.getInstance();
      userEntity = userController.getUser(username.toUpperCase());
      user = new User(username, userEntity.getBpnm(), "deneme@mail", new Date(), new Date());
    }
    catch (NullPointerException e)
    {
      System.out.println(userEntity.getBpid() + " " + userEntity.getBpnm() + " " + username);
    }
    return user;
  }
  
  public User createUser(String username, String password, String name, String email)
    throws UserAlreadyExistsException
  {
    try
    {
      loadUser(username);
      
      throw new UserAlreadyExistsException("Username " + username + " already exists");
    }
    catch (UserNotFoundException unfe)
    {
      Date now = new Date();
      Connection con = null;
      PreparedStatement pstmt = null;
      try
      {
        con = DbConnectionManager.getConnection();
        pstmt = con.prepareStatement("INSERT INTO ofUser (username,name,email,creationDate,modificationDate) VALUES (?,?,?,?,?)");
        pstmt.setString(1, username);
        if ((name == null) || (name.matches("\\s*"))) {
          pstmt.setNull(2, 12);
        } else {
          pstmt.setString(2, name);
        }
        if ((email == null) || (email.matches("\\s*"))) {
          pstmt.setNull(3, 12);
        } else {
          pstmt.setString(3, email);
        }
        pstmt.setString(4, StringUtils.dateToMillis(now));
        pstmt.setString(5, StringUtils.dateToMillis(now));
        pstmt.execute();
      }
      catch (SQLException e)
      {
        throw new RuntimeException(e);
      }
      finally
      {
        DbConnectionManager.closeConnection(pstmt, con);
      }
      try
      {
        AuthFactory.setPassword(username, password);
      }
      catch (Exception e)
      {
        Log.error("User pasword not set", e);
      }
      return new User(username, name, email, now, now);
    }
  }
  
  public void deleteUser(String username)
  {
    Connection con = null;
    PreparedStatement pstmt = null;
    boolean abortTransaction = false;
    try
    {
      con = DbConnectionManager.getTransactionConnection();
      pstmt = con.prepareStatement("DELETE FROM ofUserProp WHERE username=?");
      pstmt.setString(1, username);
      pstmt.execute();
      DbConnectionManager.fastcloseStmt(pstmt);
      
      pstmt = con.prepareStatement("DELETE FROM ofUserFlag WHERE username=?");
      pstmt.setString(1, username);
      pstmt.execute();
      DbConnectionManager.fastcloseStmt(pstmt);
      
      pstmt = con.prepareStatement("DELETE FROM ofUser WHERE username=?");
      pstmt.setString(1, username);
      pstmt.execute();
    }
    catch (Exception e)
    {
      Log.error(e.getMessage(), e);
      abortTransaction = true;
    }
    finally
    {
      DbConnectionManager.closeStatement(pstmt);
      DbConnectionManager.closeTransactionConnection(pstmt, con, abortTransaction);
    }
  }
  
  public int getUserCount()
  {
    int count = 0;
    Connection con = null;
    PreparedStatement pstmt = null;
    ResultSet rs = null;
    try
    {
      con = DbConnectionManager.getConnection();
      pstmt = con.prepareStatement("SELECT count(*) FROM ofUser");
      rs = pstmt.executeQuery();
      if (rs.next()) {
        count = rs.getInt(1);
      }
    }
    catch (SQLException e)
    {
      Log.error(e.getMessage(), e);
    }
    finally
    {
      DbConnectionManager.closeConnection(rs, pstmt, con);
    }
    return count;
  }
  
  public Collection<User> getUsers()
  {
    Collection<String> usernames = getUsernames(0, Integer.MAX_VALUE);
    return new UserCollection((String[])usernames.toArray(new String[usernames.size()]));
  }
  
  public Collection<String> getUsernames()
  {
    return getUsernames(0, Integer.MAX_VALUE);
  }
  
  private Collection<String> getUsernames(int startIndex, int numResults)
  {
    List<String> usernames = new ArrayList();
    
    UserController userController = UserController.getInstance();
    for (UserEntity userEntity : userController.getUsers()) {
      if (userEntity.getBpid().length() > 1) {
        usernames.add(userEntity.getBpid());
      }
    }
    return usernames;
  }
  
  public Collection<User> getUsers(int startIndex, int numResults)
  {
    Collection<String> usernames = getUsernames(startIndex, numResults);
    return new UserCollection((String[])usernames.toArray(new String[usernames.size()]));
  }
  
  public void setName(String username, String name)
    throws UserNotFoundException
  {
    Connection con = null;
    PreparedStatement pstmt = null;
    try
    {
      con = DbConnectionManager.getConnection();
      pstmt = con.prepareStatement("UPDATE ofUser SET name=? WHERE username=?");
      if ((name == null) || (name.matches("\\s*"))) {
        pstmt.setNull(1, 12);
      } else {
        pstmt.setString(1, name);
      }
      pstmt.setString(2, username);
      pstmt.executeUpdate();
    }
    catch (SQLException sqle)
    {
      throw new UserNotFoundException(sqle);
    }
    finally
    {
      DbConnectionManager.closeConnection(pstmt, con);
    }
  }
  
  public void setEmail(String username, String email)
    throws UserNotFoundException
  {
    Connection con = null;
    PreparedStatement pstmt = null;
    try
    {
      con = DbConnectionManager.getConnection();
      pstmt = con.prepareStatement("UPDATE ofUser SET email=? WHERE username=?");
      if ((email == null) || (email.matches("\\s*"))) {
        pstmt.setNull(1, 12);
      } else {
        pstmt.setString(1, email);
      }
      pstmt.setString(2, username);
      pstmt.executeUpdate();
    }
    catch (SQLException sqle)
    {
      throw new UserNotFoundException(sqle);
    }
    finally
    {
      DbConnectionManager.closeConnection(pstmt, con);
    }
  }
  
  public void setCreationDate(String username, Date creationDate)
    throws UserNotFoundException
  {
    Connection con = null;
    PreparedStatement pstmt = null;
    try
    {
      con = DbConnectionManager.getConnection();
      pstmt = con.prepareStatement("UPDATE ofUser SET creationDate=? WHERE username=?");
      pstmt.setString(1, StringUtils.dateToMillis(creationDate));
      pstmt.setString(2, username);
      pstmt.executeUpdate();
    }
    catch (SQLException sqle)
    {
      throw new UserNotFoundException(sqle);
    }
    finally
    {
      DbConnectionManager.closeConnection(pstmt, con);
    }
  }
  
  public void setModificationDate(String username, Date modificationDate)
    throws UserNotFoundException
  {
    Connection con = null;
    PreparedStatement pstmt = null;
    try
    {
      con = DbConnectionManager.getConnection();
      pstmt = con.prepareStatement("UPDATE ofUser SET modificationDate=? WHERE username=?");
      pstmt.setString(1, StringUtils.dateToMillis(modificationDate));
      pstmt.setString(2, username);
      pstmt.executeUpdate();
    }
    catch (SQLException sqle)
    {
      throw new UserNotFoundException(sqle);
    }
    finally
    {
      DbConnectionManager.closeConnection(pstmt, con);
    }
  }
  
  public Set<String> getSearchFields()
    throws UnsupportedOperationException
  {
    return new LinkedHashSet(Arrays.asList(new String[] { "Username", "Name", "Email" }));
  }
  
  public Collection<User> findUsers(Set<String> fields, String query)
    throws UnsupportedOperationException
  {
    return findUsers(fields, query, 0, Integer.MAX_VALUE);
  }
  
  public Collection<User> findUsers(Set<String> fields, String query, int startIndex, int numResults)
    throws UnsupportedOperationException
  {
    if (fields.isEmpty()) {
      return Collections.emptyList();
    }
    if (!getSearchFields().containsAll(fields)) {
      throw new IllegalArgumentException("Search fields " + fields + " are not valid.");
    }
    if ((query == null) || ("".equals(query))) {
      return Collections.emptyList();
    }
    query = "%" + query.replace('*', '%') + "%";
    if (query.endsWith("%%")) {
      query = query.substring(0, query.length() - 1);
    }
    List<String> usernames = new ArrayList(50);
    Connection con = null;
    PreparedStatement pstmt = null;
    int queries = 0;
    ResultSet rs = null;
    try
    {
      StringBuilder sql = new StringBuilder(90);
      sql.append("SELECT username FROM ofUser WHERE");
      boolean first = true;
      if (fields.contains("Username"))
      {
        sql.append(" username LIKE ?");
        queries++;
        first = false;
      }
      if (fields.contains("Name"))
      {
        if (!first) {
          sql.append(" AND");
        }
        sql.append(" name LIKE ?");
        queries++;
        first = false;
      }
      if (fields.contains("Email"))
      {
        if (!first) {
          sql.append(" AND");
        }
        sql.append(" email LIKE ?");
        queries++;
      }
      con = DbConnectionManager.getConnection();
      if ((startIndex == 0) && (numResults == Integer.MAX_VALUE))
      {
        pstmt = con.prepareStatement(sql.toString());
        for (int i = 1; i <= queries; i++) {
          pstmt.setString(i, query);
        }
        rs = pstmt.executeQuery();
      }
      while (rs.next())
      {
        usernames.add(rs.getString(1)); continue;
      }
      if (Log.isDebugEnabled())
      {
        Log.debug("Results: " + usernames.size());
        LogResults(usernames);
      }
    }
    catch (SQLException e)
    {
      Log.error(e.getMessage(), e);
    }
    finally
    {
      DbConnectionManager.closeConnection(rs, pstmt, con);
    }
    return new UserCollection((String[])usernames.toArray(new String[usernames.size()]));
  }
  
  public boolean isReadOnly()
  {
    return false;
  }
  
  public boolean isNameRequired()
  {
    return false;
  }
  
  public boolean isEmailRequired()
  {
    return false;
  }
  
  private void LogResults(List<String> listElements)
  {
    String callingMethod = Thread.currentThread().getStackTrace()[3].getMethodName();
    StringBuilder sb = new StringBuilder(256);
    int count = 0;
    for (String element : listElements)
    {
      if (count > 20)
      {
        Log.debug(callingMethod + " results: " + sb.toString());
        sb.delete(0, sb.length());
        count = 0;
      }
      sb.append(element).append(',');
      count++;
    }
    sb.append('.');
    Log.debug(callingMethod + " results: " + sb.toString());
  }
}
