package bnet.detaysoft.com;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Date;
import org.jivesoftware.openfire.XMPPServer;
import org.jivesoftware.openfire.container.Plugin;
import org.jivesoftware.openfire.container.PluginManager;
import org.jivesoftware.util.JiveGlobals;

public class BnetProvidersPlugin
  implements Plugin
{
  public void initializePlugin(PluginManager manager, File pluginDirectory)
  {
    propertySetup();
    fileConfig();
  }
  
  public void destroyPlugin() {}
  
  private void propertySetup() {}
  
  private void fileConfig()
  {
    File sourceLib = new File("..\\plugins\\bnetproviders\\lib\\bnetproviders-lib.jar");
    
    String modifyDateTime = String.valueOf(new Date(sourceLib.lastModified()).getTime());
    if ((!modifyDateTime.equals("0")) && 
      (!JiveGlobals.getProperty("dty.dop.modify.time", "0").equalsIgnoreCase(modifyDateTime)))
    {
      System.out.println("Yeni plugin testpit edildi...");
      File destLib = new File("..\\lib\\BnetProviders.jar");
      try
      {
        destLib.delete();
      }
      catch (Exception localException) {}
      try
      {
        copyFile(sourceLib, destLib);
      }
      catch (IOException e)
      {
        XMPPServer.getInstance().stop();
      }
      JiveGlobals.setProperty("dty.dop.modify.time", 
        String.valueOf(new Date(sourceLib.lastModified()).getTime()));
      System.out.println("Yeniden baslatma gerekli");
      XMPPServer.getInstance().stop();
    }
  }
  
  private static void copyFile(File source, File dest)
    throws IOException
  {
    FileInputStream input = null;
    FileOutputStream output = null;
    try
    {
      input = new FileInputStream(source);
      output = new FileOutputStream(dest);
      byte[] buf = new byte[1024];
      int bytesRead;
      while ((bytesRead = input.read(buf)) > 0) {
        output.write(buf, 0, bytesRead);
      }
    }
    catch (Exception localException) {}finally
    {
      input.close();
      output.close();
    }
  }
}
