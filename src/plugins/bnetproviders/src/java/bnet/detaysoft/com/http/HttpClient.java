package bnet.detaysoft.com.http;

import com.google.gson.JsonObject;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

import bnet.detaysoft.com.utils.BnetModuleAbstract;

import java.io.UnsupportedEncodingException;
import java.util.List;
import org.apache.commons.codec.binary.Base64;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.jivesoftware.util.JiveGlobals;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HttpClient extends BnetModuleAbstract {

	private static final Logger Log = LoggerFactory.getLogger(HttpClient.class);

	/**
	 * .NET user servislerinden kullanicilarin cekildigi metod
	 * 
	 * @return
	 */
	public String userDataRequest() {
		HttpResponse<String> response = null;

		try {

			response = Unirest.post("http://bnet.detaysoft.com/services/PersonService.svc/GetPerson")
					.header("authorization", "key " + getServiceSecret()).header("content-type", "application/json")
					.body("{\"Filter\": [{\"PropertyName\": \"ACTIVE\",\"Operation\": \"EQ\",\"PropertyValue\": \"X\"},{\"PropertyName\": \"BPTP\",\"Operation\": \"EQ\",\"PropertyValue\": \"BP\"}],\"FilterCompareType\":\"AND\",\"ReturnDtoType\":\"PersonelMasterModelDto\"}")
					.asString();
		} catch (UnirestException e) {
			Log.error("get user data error: ", e);
		}
		return (String) response.getBody();
	}

	/**
	 * 
	 * Keycloaktan alınan token
	 * 
	 * @return
	 */
	private String getKeycloakToken() {

		String username = JiveGlobals.getProperty("bnet.keycloak.user", "p1202");
		String userpass = JiveGlobals.getProperty("bnet.keycloak.psw", "enlic@y123");
		String authurl = JiveGlobals.getProperty("bnet.keycloak.tokenUrl",
				"http://bnet.detaysoft.com/auth/realms/master/protocol/openid-connect/token");
		HttpResponse<String> response = null;
		try {
			response = Unirest.post(authurl).header("content-type", "application/x-www-form-urlencoded")
					.body("client_id=admin-cli&grant_type=password&username=" + username + "&password=" + userpass)
					.asString();
		} catch (Exception e) {
			Log.error("get keycloak token error: ", e);
		}
		JsonObject keycloakObject = getProperty(response.getBody());
		return getString(keycloakObject.get("access_token"));
	}

	public boolean validateUserOnKeycloak(String token) {

		String validateUrl = JiveGlobals.getProperty("bnet.keycloak.validateUrl",
				"http://bnet.detaysoft.com/auth/realms/master/protocol/openid-connect/token/introspect");

		boolean isStatus = false;
		try {
			String clientId = JiveGlobals.getProperty("bnet.keycloak.clientid", "test");

			String authHeader = "account:" + clientId;

			HttpResponse<String> response = Unirest.post(validateUrl)
					.header("content-type", "application/x-www-form-urlencoded")
					.header("authorization", "Basic " + new String(Base64.encodeBase64(authHeader.getBytes("UTF-8"))))
					.body("token=" + token).asString();
			if (response.getStatus() == 200) {
				isStatus = true;
			}
		} catch (Exception e) {
			Log.error("validate keycloak token error : ", e);
		}
		return isStatus;
	}

}
