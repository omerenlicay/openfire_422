package bnet.detaysoft.com.utils;

import java.io.UnsupportedEncodingException;

import org.apache.commons.codec.binary.Base64;

import com.google.gson.JsonElement;
import com.google.gson.JsonNull;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public abstract class BnetModuleAbstract {

	/**
	 * Kullanicidan gelen json verinin parse edildigi metod.
	 * 
	 * @param jsonObject
	 * @return
	 */
	public static JsonObject getProperty(String jsonObject) {
		// Gelen parametler JsonObject'e parse ediliyor
		JsonParser jsonParser = new JsonParser();
		JsonObject object = jsonParser.parse(jsonObject).getAsJsonObject();
		return object;
		
	}
	
	
	/**
	 * Gelen parametlerin filtreden gectigi yer
	 *
	 * @param element
	 * @return
	 */
	protected static String getString(JsonElement element) {
		String returnString = "";
		if (element != null && !(element instanceof JsonNull)) {
			returnString = element.getAsString();
			returnString = returnString.replaceAll("'", "\'");
		}
		return returnString;
	}

	/**
	 * Verinin base64 ten String e don�st�r�ld�g� metod.
	 * 
	 * @param base64Data
	 * @return
	 */
	public static String decodeBase64(String base64Data) {
		String returnData = null;
		try {
			byte[] byteData = Base64.decodeBase64(base64Data.getBytes("UTF-8"));
			returnData = new String(byteData, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return returnData;
	}

	/**
	 * Verinin base64 e donusturuldugu metod.
	 * 
	 * @param base64Data
	 * @return
	 */
	public static String encodeBase64(String base64Data) {
		String returnData = null;
		try {
			byte[] byteData = Base64.encodeBase64(base64Data.getBytes("UTF-8"));
			returnData = new String(byteData, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return returnData;
	}
	
	/**
	 * Verinin base64 e donusturulup compresslendigi metod
	 * 
	 * @param base64Data
	 * @return
	 */
	public static String encodeBase64WCompress(String base64Data) {
		String returnData = null;
		Compress compress = new Compress();
		try {
			byte[] byteData = Base64.encodeBase64(compress.compress(base64Data.getBytes("UTF-8")));
			returnData = new String(byteData, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return returnData;
	}
	
	
	public String getServiceSecret()
	{
		return encodeBase64("100:OPENFIRE:bobili");
	}




}
